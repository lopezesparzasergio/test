# Con clousures
	var filtraPorEdad = function (coleccion, edad) {
      var filtrar = function(){
        return coleccion.filter(item => {
          return item.edad >= edad;
        }
      )}
      return filtrar;
    }

    // Con el resultado de la función anterior implementar la siguiente.
    var marcarHabilitados = function (coleccion) {
      var marcar = function(){
        return coleccion.map(item => {
          return Object.assign({}, item, {
            habilitado: true
          });
        });
      }
      return marcar;
    }
    
    var coleccionOriginal = [{edad:18}, {edad: 19}, {edad: 25}, {edad: 5},{edad: 6}];
    var filtrado = filtraPorEdad(coleccionOriginal, 15);
    var marcar = marcarHabilitados(filtrado());
    var objetosMarcados = marcar();

    console.log(objetosMarcados);
    console.log(coleccionOriginal);

# Sin clousures, modificando funcion marcarHabilitados

	function filtraPorEdad (coleccion, edad) {
		return coleccion.filter(item => {
		     return item.edad >= edad;
	  })
	}

	// Con el resultado de la función anterior implementar la siguiente.
	function marcarHabilitados(coleccion) {
		return coleccion.map(item => {
		    return Object.assign({}, item, {
			    habilitado: true
		     });
		});
	}

	var coleccionOriginal = [{edad:18}, {edad: 19}, {edad: 25}, {edad: 5},{edad: 6}];
	var filtrados = filtraPorEdad(coleccionOriginal, 14);
	var marcados = marcarHabilitados(filtrados);

	console.log(marcados);
	console.log(coleccionOriginal);